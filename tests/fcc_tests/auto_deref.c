#include <stdio.h>
struct person
{
	const char *name;
};

void print_name(struct person *p)
{
	printf("This person's name is %s\n", p.name);
}

void print_name_arrow(struct person *p)
{
	printf("This person's name is %s\n", p->name);
}

int main(void)
{
	struct person George;
	George.name = "George";

	print_name(&George);
	print_name_arrow(&George);

	return 0;
}
